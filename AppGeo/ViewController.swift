//
//  ViewController.swift
//  AppGeo
//
//  Created by WCS on 8/11/18.
//  Copyright © 2018 WCS. All rights reserved.
//

import UIKit
//import MapKit
import GoogleMaps
import UserNotifications
import CoreLocation



class ViewController: UIViewController, CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPosiciones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbNotificaciones.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = arrayPosiciones[indexPath.row]
        return cell

    }
    
    

    //private  var locationManager = CLLocationManager ()
    var locationManager : CLLocationManager?

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapCenterPinImage: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var tbNotificaciones: UITableView!
    @IBOutlet weak var geofencesLabel: UILabel!
    //@IBOutlet weak var mapView: MKMapView!
    var arrayPosiciones:[String] = []
    
    @IBAction func toggleGeofences(_ sender: Any) {
        if let locationManager = self.locationManager {
            let region = self.regionToMonitor()
            if(locationManager.monitoredRegions.count > 0) {
                self.geofencesLabel.text = "Geofences OFF"
                locationManager.stopMonitoring(for: region)
                //mapView.removeOverlays(mapView.overlays)
            } else {
                self.geofencesLabel.text = "Geofences ON"
                locationManager.startMonitoring(for: region)
                //mapView.add(MKCircle(center: region.center, radius: region.radius))
            }
        } else {
            notify(msg: "Unable to set geofence")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbNotificaciones.dataSource = self
        tbNotificaciones.delegate = self
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            locationManager = appDelegate.locationManager
            //locationManager?.delegate = self as! CLLocationManagerDelegate
            locationManager?.delegate = self
            locationManager?.requestWhenInUseAuthorization()
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            locationManager?.startUpdatingLocation()
        }
        //locationManager.delegate = self
        //locationManager.requestWhenInUseAuthorization()
       
        mapView.delegate = self as? GMSMapViewDelegate
        mapView.mapType = .normal
        let camera = GMSCameraPosition.camera(withLatitude: -16.411662,
                                              longitude: -71.521004,
                                              zoom: 15.0)
        mapView.camera = camera
        mapView.animate(to: camera)
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        let circleCenter = CLLocationCoordinate2D(latitude: -16.411662, longitude: -71.521004)
        let circ = GMSCircle(position:  circleCenter, radius: 70)
        circ.map = mapView
        
        let circleCenter2 = CLLocationCoordinate2D(latitude: -16.413834, longitude: -71.520836)
        let circ2 = GMSCircle(position:  circleCenter2, radius: 35)
        circ2.map = mapView
    
        let region = self.regionToMonitor()
        locationManager?.startMonitoring(for: region)
    }
    func notify(msg : String) {
        let content = UNMutableNotificationContent()
        content.title = "Autentia"
        content.body = msg
        let request = UNNotificationRequest(identifier: "geofence", content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    // MARK: - CLLocationManagerDelegagte methods
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        //notify(msg: "Hello from Autentia")
        arrayPosiciones.append("ingreso a zona")
        tbNotificaciones.reloadData()

    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        //notify(msg: "Bye from Autentia")
        arrayPosiciones.append("salida a zona")
        tbNotificaciones.reloadData()

    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
    func regionToMonitor() -> CLCircularRegion {
        let autentia = CLCircularRegion(center: CLLocationCoordinate2D(latitude: -16.413834, longitude: -71.520836), radius: 100, identifier: "autentia")
        autentia.notifyOnExit = true
        autentia.notifyOnEntry = true
        return autentia
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        // 1
        let geocoder = GMSGeocoder()
        // 1
        let labelHeight = self.addressLabel.intrinsicContentSize.height
        self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                            bottom: labelHeight, right: 0)

        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            // 3
            self.addressLabel.text = lines.joined(separator: "\n")
            
            // 4
            UIView.animate(withDuration: 0.25) {
                //2
                //self.mapCenterPinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                //self.mapCenterPinImage
                self.view.layoutIfNeeded()
            }

        }
    }

}
// MARK: - GMSMapViewDelegate
extension ViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        //addressLabel.lock()
    }


}



// MARK: - CLLocationManagerDelegate
//1

/*extension ViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        addressLabel.lock()
        
        if (gesture) {
            mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let placeMarker = marker as? PlaceMarker else {
            return nil
        }
        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
            return nil
        }
        
        infoView.nameLabel.text = placeMarker.place.name
        if let photo = placeMarker.place.photo {
            infoView.placePhoto.image = photo
        } else {
            infoView.placePhoto.image = UIImage(named: "generic")
        }
        
        return infoView
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
}*/

